package it.unibo.sd1819.lab9.examples;

import java.util.concurrent.*;

public class AsyncCounter {
    private static final ExecutorService engine = Executors.newSingleThreadExecutor();
    private int index;

    public AsyncCounter(int startingValue) {
        index = startingValue;
    }

    public Future<Void> reach(int target) {
        final CompletableFuture<Void> termination = new CompletableFuture<>();
        termination.thenRun(() -> log("Reached the target: %d", target));
        log("Reaching the target %d ...", target);
        doCount(target, termination);
        return termination;
    }

    private void doCount(int target, CompletableFuture<Void> termination) {
        if (index < target)  {
            engine.submit(() -> count(+1, target, termination));
        } else if (index > target) {
            engine.submit(() -> count(-1, target, termination));
        } else {
            termination.complete(null);
        }
    }

    private void count(int delta, int target, CompletableFuture<Void> termination) {
        final int oldValue = index;
        index += delta;
        doCount(target, termination);  // Recursion here!
        log("(index=%d) -> (index=%d)", oldValue, index);
    }

    private void log(String format, Object... args) {
        System.out.printf("[" + this.toString() + "] " + format + "\n", args);
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        AsyncCounter c1 = new AsyncCounter(100);
        AsyncCounter c2 = new AsyncCounter(-100);

        Future<?> c1Termination = c1.reach(0);
        Future<?> c2Termination = c2.reach(0);

        c1Termination.get();
        c2Termination.get();

        System.exit(0);
    }
}
