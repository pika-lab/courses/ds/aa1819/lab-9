package it.unibo.sd1819.lab9.ts.logic;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import it.unibo.sd1819.lab9.agents.Agent;
import it.unibo.sd1819.lab9.agents.DistributedEnvironment;
import it.unibo.sd1819.lab9.agents.Environment;
import it.unibo.sd1819.lab9.tusow.TuSoWService;
import org.junit.*;

import it.unibo.sd1819.test.ConcurrentTestHelper;

// TODO notice that my tests use the Agent class...
public class TestLogicTupleSpace {

    private static final Duration MAX_WAIT = Duration.ofSeconds(3);

    protected ConcurrentTestHelper test;
    protected Random rand;
    protected Environment mas;

    @BeforeClass
    public static void setUpClass() throws Exception {
        TuSoWService.start("-p", "8080", "-r", "tuple-spaces");
    }

    @Before
    public void setUp() throws Exception {
        test = new ConcurrentTestHelper();
        rand = new Random();
        mas = new DistributedEnvironment("localhost", 8080, "tuple-spaces");
    }

    @After
    public void tearDown() throws InterruptedException, ExecutionException, TimeoutException {
        mas.shutdown().awaitShutdown(MAX_WAIT);
    }

    @AfterClass
    public static void tearDownClass() throws InterruptedException, ExecutionException, TimeoutException {
        TuSoWService.stop();
    }

    @Test
    public void testInitiallyEmpty() throws Exception {
        test.setThreadCount(1);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testInitiallyEmpty") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0);
                stopNow();
            }

        }, true);


        test.await();
        alice.await();
    }


    @Test
    public void testReadSuspensiveSemantics() throws Exception {
        test.setThreadCount(1);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testReadSuspensiveSemantics") {
            @Override
            public void onRun() {
                test.assertBlocksIndefinitely(tupleSpace.read("f(X)"), "A read operation should block if no tuple matching the requested template is available");
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }


    @Test
    public void testTakeSuspensiveSemantics() throws Exception {
        test.setThreadCount(1);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testTakeSuspensiveSemantics") {
            @Override
            public void onRun() {
                test.assertBlocksIndefinitely(tupleSpace.take("f(x)"), "A take operation should block if no tuple matching the requested template is available");
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }


    @Test
    public void testWriteGenerativeSemantics() throws Exception {
        test.setThreadCount(1);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testWriteGenerativeSemantics") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0, "The tuple space must initially be empty");
                test.assertEquals(tupleSpace.write("s(z)"), new LogicTuple("s(z)"), "A write operation eventually return the same tuple it received as argument");
                test.assertEquals(tupleSpace.getSize(), 1, "After a tuple was written, the tuple space size should increase");
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }


    @Test
    public void testReadIsIdempotent1() throws Exception {
        test.setThreadCount(2);

        final LogicTuple tuple = new LogicTuple("s(z)");


        Agent bob = mas.registerAgent(new TestAgent("Bob", "testReadIsIdempotent1") {

            int i = rand.nextInt(10) + 1;

            @Override
            public void onRun() {
                if (i-- >= 0) {
                    test.assertEquals(tupleSpace.read("s(X)"), tuple);
                } else {
                    test.assertEquals(tupleSpace.read("s(Y)"), tuple);
                    stopNow();
                }
            }

        }, false);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testReadIsIdempotent1") {
            @Override
            public void onRun() {
                test.assertEventuallyReturns(tupleSpace.write(tuple));
                bob.start();
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
        bob.await();
    }

    @Test
    public void testReadIsIdempotent2() throws Exception {
        test.setThreadCount(2);

        final LogicTuple tuple = new LogicTuple("s(z)");

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testReadIsIdempotent2") {
            @Override
            public void onRun() {
                test.assertEventuallyReturns(tupleSpace.write(tuple));
                stopNow();
            }
        }, false);

        Agent bob = mas.registerAgent(new TestAgent("Bob", "testReadIsIdempotent2") {
            @Override
            public void onRun() {
                final Future<LogicTuple> toBeRead1 = tupleSpace.read("s(X)");
                final Future<LogicTuple> toBeRead2 = tupleSpace.read("s(Y)");

                alice.start();

                test.assertEquals(toBeRead1, tuple);
                test.assertEquals(toBeRead2, tuple);

                stopNow();
            }
        }, true);

        test.await();
        alice.await();
        bob.await();
    }

    @Test
    public void testTakeIsNotIdempotent1() throws Exception {
        test.setThreadCount(2);

        final LogicTuple tuple = new LogicTuple("foo(bar)");

        Agent bob = mas.registerAgent(new TestAgent("Bob", "testTakeIsNotIdempotent1") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.take("foo(X)"), tuple);
                test.assertBlocksIndefinitely(tupleSpace.take("foo(_)"));
                stopNow();
            }
        }, false);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testTakeIsNotIdempotent1") {
            @Override
            public void onRun() {
                test.assertEventuallyReturns(tupleSpace.write(tuple));
                bob.start();
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
        bob.await();
    }

    @Test
    public void testTakeIsNotIdempotent2() throws Exception {
        test.setThreadCount(2);

        final LogicTuple tuple = new LogicTuple("foo(bar)");

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testTakeIsNotIdempotent2") {
            @Override
            public void onRun() {
                test.assertEventuallyReturns(tupleSpace.write(tuple));
                stopNow();
            }
        }, false);

        Agent bob = mas.registerAgent(new TestAgent("Bob", "testTakeIsNotIdempotent2") {
            @Override
            public void onRun() {
                Future<LogicTuple> toBeWritten = tupleSpace.take("foo(X)");
                alice.start();
                test.assertEquals(toBeWritten, tuple);
                test.assertBlocksIndefinitely(tupleSpace.take("foo(_)"));
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
        bob.await();
    }

    @Test
    public void testAssociativeAccess() throws Exception {
        test.setThreadCount(3);

        final LogicTuple tuple4Bob = new LogicTuple("msg(to(bob), hi_bob)");
        final LogicTuple tuple4Carl = new LogicTuple("msg(to(carl), hi_carl)");

        Agent carl = mas.registerAgent(new TestAgent("Carl", "testAssociativeAccess") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.read("msg(to(carl), M)"), tuple4Carl);
                stopNow();
            }
        }, true);

        Agent bob = mas.registerAgent(new TestAgent("Bob", "testAssociativeAccess") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.read("msg(to(bob), M)"), tuple4Bob);
                stopNow();
            }
        }, true);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testAssociativeAccess") {
            @Override
            public void onRun() {
                test.assertEventuallyReturns(tupleSpace.write(tuple4Bob));
                test.assertEventuallyReturns(tupleSpace.write(tuple4Carl));

                test.assertOneOf(tupleSpace.take("msg(to(_), M)"), tuple4Bob, tuple4Carl);
                test.assertOneOf(tupleSpace.take("msg(to(_), M)"), tuple4Bob, tuple4Carl);
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
        bob.await();
        carl.await();
    }

    @Test
    public void testGetSize() throws Exception {
        test.setThreadCount(1);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testGetSize") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0);
                test.assertEventuallyReturns(tupleSpace.write("a"));
                test.assertEquals(tupleSpace.getSize(), 1);
                test.assertEventuallyReturns(tupleSpace.write("a"));
                test.assertEquals(tupleSpace.getSize(), 2);
                test.assertEventuallyReturns(tupleSpace.write("a"));
                test.assertEquals(tupleSpace.getSize(), 3);

                test.assertEventuallyReturns(tupleSpace.take("a"));
                test.assertEquals(tupleSpace.getSize(), 2);
                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }

    @Test
    public void testGetAll() throws Exception {
        test.setThreadCount(1);

        final List<LogicTuple> expected = new ArrayList<>(Arrays.asList(
                new LogicTuple("b"),
                new LogicTuple("c"),
                new LogicTuple("a"),
                new LogicTuple("b")
        ));

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testGetAll") {

            Queue<LogicTuple> queue = new LinkedList<>(expected);

            @Override
            public void onRun() {
                if (queue.isEmpty()) {
                    test.assertEquals(tupleSpace.get(), expected);
                    stopNow();
                } else {
                    test.assertEventuallyReturns(tupleSpace.write(queue.poll()));
                }
            }
        }, true);

        test.await();
        alice.await();
    }


    @Test
    public void testWriteAll() throws Exception {
        test.setThreadCount(1);

        final List<LogicTuple> tuples = Arrays.asList(
                new LogicTuple("b(2)"),
                new LogicTuple("c(3)"),
                new LogicTuple("a(1)"),
                new LogicTuple("b(2)")
        );

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testWriteAll") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0);
                test.assertEquals(tupleSpace.writeAll(tuples), tuples);
                test.assertEquals(tupleSpace.getSize(), tuples.size());
                test.assertEquals(tupleSpace.get(), tuples);

                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }

    @Test
    public void testWriteAllResumesSuspendedOperations() throws Exception {
        test.setThreadCount(2);

        final List<LogicTuple> tuples = Stream.of("f(x)", "f(y)", "g(x)", "g(y)")
                .map(LogicTuple::new)
                .collect(Collectors.toList());

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testWriteAllResumesSuspendedOperations") {
            @Override
            public void onRun() {
                test.assertEventuallyReturns(tupleSpace.writeAll(tuples));

                stopNow();
            }
        }, false);

        Agent bob = mas.registerAgent(new TestAgent("Bob", "testWriteAllResumesSuspendedOperations") {
            @Override
            public void onRun() {
                final Future<LogicTuple> toBeRead = tupleSpace.read("f(A)");
                final Future<LogicTuple> toBeTaken = tupleSpace.take("g(A)");

                alice.start();

                test.assertOneOf(toBeRead, tuples.get(0), tuples.get(1));
                test.assertOneOf(toBeTaken, tuples.get(2), tuples.get(3));

                stopNow();
            }
        }, true);

        test.await();
        alice.await();
        bob.await();
    }

    @Test
    public void testReadAll() throws Exception {
        test.setThreadCount(1);

        final List<LogicTuple> tuples = Arrays.asList(
                new LogicTuple("a(2)"),
                new LogicTuple("a(3)"),
                new LogicTuple("a(1)"),
                new LogicTuple("a(4)"),
                new LogicTuple("b(5)")
        );

        final Collection<LogicTuple> expected = tuples.subList(0, 4);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testReadAll") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0);
                test.assertEventuallyReturns(tupleSpace.writeAll(tuples));
                test.assertEquals(tupleSpace.getSize(), tuples.size());
                test.assertEquals(tupleSpace.readAll("a(N)"), expected);
                test.assertEquals(tupleSpace.readAll("a(N)"), expected);
                test.assertEquals(tupleSpace.getSize(), tuples.size());

                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }

    @Test
    public void testTryRead() throws Exception {
        test.setThreadCount(1);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testTryRead") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0);
                test.assertEventuallyReturns(tupleSpace.write("p(a)"));
                test.assertEquals(tupleSpace.getSize(), 1);
                test.assertEquals(tupleSpace.tryRead("p(Z)"), Optional.of(new LogicTuple("p(a)")));
                test.assertEquals(tupleSpace.getSize(), 1);
                test.assertEquals(tupleSpace.tryRead("p(Z)"), Optional.of(new LogicTuple("p(a)")));
                test.assertEquals(tupleSpace.getSize(), 1);

                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }

    @Test
    public void testTryTake() throws Exception {
        test.setThreadCount(1);

        Agent alice = mas.registerAgent(new TestAgent("Alice", "testTryTake") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0);
                test.assertEventuallyReturns(tupleSpace.write("p(a)"));
                test.assertEquals(tupleSpace.getSize(), 1);
                test.assertEquals(tupleSpace.tryTake("p(Z)"), Optional.of(new LogicTuple("p(a)")));
                test.assertEquals(tupleSpace.getSize(), 0);
                test.assertEquals(tupleSpace.tryTake("p(Z)"), Optional.empty());

                stopNow();
            }
        }, true);


        test.await();
        alice.await();
    }

    @Test
    public void testTakeAll() throws Exception {
        test.setThreadCount(1);

        final List<LogicTuple> tuples = Arrays.asList(
                new LogicTuple("a(2)"),
                new LogicTuple("a(3)"),
                new LogicTuple("a(1)"),
                new LogicTuple("a(4)"),
                new LogicTuple("b(5)")
        );

        final Collection<LogicTuple> expected = tuples.subList(0, 4);


        Agent alice = mas.registerAgent(new TestAgent("Alice", "testTakeAll") {
            @Override
            public void onRun() {
                test.assertEquals(tupleSpace.getSize(), 0);
                test.assertEventuallyReturns(tupleSpace.writeAll(tuples));
                test.assertEquals(tupleSpace.getSize(), tuples.size());
                test.assertEquals(tupleSpace.takeAll("a(N)"), expected);
                test.assertEquals(tupleSpace.takeAll("a(N)"), Collections.emptySet());
                test.assertEquals(tupleSpace.getSize(), 1);

                stopNow();
            }
        }, true);

        test.await();
        alice.await();
    }

    private abstract class TestAgent extends Agent {

        private final String tupleSpaceName;
        protected LogicTupleSpace tupleSpace;

        protected TestAgent(String name, String tupleSpaceName) {
            super(name);
            this.tupleSpaceName = tupleSpaceName;
        }

        @Override
        public void onBegin() {
            tupleSpace = getEnvironment().getTupleSpace(tupleSpaceName);
        }

        @Override
        public AndThen onUncaughtError(Exception e) {
            test.fail(e);
            return AndThen.STOP;
        }

        @Override
        public void onEnd() {
            TestLogicTupleSpace.this.test.done();
        }
    }

}
