package it.unibo.sd1819.lab9.agents;

import it.unibo.sd1819.lab9.exercises.PongAgent;
import it.unibo.sd1819.lab9.ts.logic.LogicTuple;
import it.unibo.sd1819.lab9.ts.logic.LogicTupleSpace;
import it.unibo.sd1819.lab9.tusow.TuSoWService;
import it.unibo.sd1819.test.ConcurrentTestHelper;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
public class TestPingPong {

    private static final Duration MAX_WAIT = Duration.ofSeconds(2);

    protected ConcurrentTestHelper test;
    protected Random rand;
    protected Environment mas;

    private final int testIndex;


    @Parameterized.Parameters
    public static Iterable<Integer> data() {
        return IntStream.range(0, 30).boxed().collect(Collectors.toList());
    }

    public TestPingPong(Integer i) {
        testIndex = i;
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        TuSoWService.start("-p", "8080", "-r", "tuple-spaces");
    }

    @Before
    public void setUp() throws Exception {
        test = new ConcurrentTestHelper();
        rand = new Random();
        // TODO notice that all agents are executed by a single thread in this test suite!
        mas = new DistributedEnvironment(Executors.newSingleThreadExecutor(), "localhost", 8080, "tuple-spaces");
    }

    @After
    public void tearDown() throws InterruptedException, ExecutionException, TimeoutException {
        mas.shutdown().awaitShutdown(MAX_WAIT);
    }

    @AfterClass
    public static void tearDownClass() throws InterruptedException, ExecutionException, TimeoutException {
        TuSoWService.stop();
    }

    // TODO readme
    @Test
    public void testPingPongExercise() throws Exception {
        final PingAgent ping = mas.createAgent(PingAgent.class, "Ping", true, "testPingPongExercise-" + testIndex);

        mas.createAgent(PongAgent.class, "Pong", true, "testPingPongExercise-" + testIndex);

        mas.awaitAllAgentsStop(MAX_WAIT);

        Assert.assertEquals(10, ping.pongCount);
        Assert.assertTrue(ping.pingTurn);
    }

    // TODO notice me!
    private static class PingAgent extends Agent {
        private int pongCount = 0;
        private boolean pingTurn = true;
        private final String channelName;
        private LogicTupleSpace channel;

        public PingAgent(String name, String channelName) {
            super(name);
            this.channelName = Objects.requireNonNull(channelName);
        }

        @Override
        public void onBegin() throws Exception {
            channel = getEnvironment().getTupleSpace(channelName);

        }

        @Override
        public void onRun() throws Exception {
            if (pongCount >= 10) {
                channel.write("msg(stopNow)").thenAcceptAsync(this::onStop, getEngine());
                pauseNow();
            } else if (pingTurn) {
                channel.write("msg(ping)").thenAcceptAsync(this::onPing, getEngine());
                pauseNow();
            } else {
                channel.take("msg(pong)").thenAcceptAsync(this::onPong, getEngine());
                pauseNow();
            }
        }

        private void onStop(LogicTuple logicTuple) {
            stop();
        }

        private void onPing(LogicTuple logicTuple) {
            log("Sent PING");
            pingTurn = false;
            resume();
        }

        private void onPong(LogicTuple logicTuple) {
            log("Received PONG");
            pongCount++;
            pingTurn = true;
            resume();
        }
    }
}
