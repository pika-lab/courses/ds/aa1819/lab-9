package it.unibo.sd1819.lab9.ts.core;

public interface Tuple {
    default boolean matches(final Template template) {
        return template.matches(this);
    }
}
