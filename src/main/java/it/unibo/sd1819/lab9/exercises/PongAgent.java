package it.unibo.sd1819.lab9.exercises;

import alice.tuprolog.Term;
import it.unibo.sd1819.lab9.agents.Agent;
import it.unibo.sd1819.lab9.ts.logic.LogicTemplate;
import it.unibo.sd1819.lab9.ts.logic.LogicTuple;
import it.unibo.sd1819.lab9.ts.logic.LogicTupleSpace;

import java.util.Objects;
import java.util.Optional;

public class PongAgent extends Agent {

    private static final String PAYLOAD_VAR = "Payaload";
    private static final LogicTemplate MSG_TEMPLATE = new LogicTemplate("msg(" + PAYLOAD_VAR +")");
    private static final Term EXIT_PAYLOAD = Term.createTerm("stopNow");

    private boolean pongTurn = false;
    private final String channelName;
    private LogicTupleSpace channel;

    public PongAgent(String name, String channelName) {
        super(name);
        this.channelName = Objects.requireNonNull(channelName);
    }

    @Override
    public void onRun() throws Exception {
        stopNow();
    }


}
