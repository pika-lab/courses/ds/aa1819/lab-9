package it.unibo.sd1819.lab9.tusow;

import alice.tuprolog.Struct;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import it.unibo.sd1819.lab9.PrologUtils;
import it.unibo.sd1819.lab9.ts.logic.LogicTemplate;
import it.unibo.sd1819.lab9.ts.logic.LogicTuple;
import it.unibo.sd1819.lab9.ts.logic.LogicTupleSpace;
import org.javatuples.Pair;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// TODO implement me!
public class RemoteLogicTupleSpace implements LogicTupleSpace {

    private final String host;
    private final int port;
    private final String root;
    private final String name;

    private final HttpClient client = null;

    public RemoteLogicTupleSpace(String host, int port, String name) {
        this(host, port, "data_spaces", name);
    }

    public RemoteLogicTupleSpace(String host, int port, String root, String name) {
        this.host = Objects.requireNonNull(host);
        this.port = port;
        this.root = Objects.requireNonNull(root);
        this.name = Objects.requireNonNull(name);
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> readAll(LogicTemplate template) {
        return request(HttpMethod.GET, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> takeAll(LogicTemplate template) {
        return request(HttpMethod.DELETE, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> writeAll(Collection<? extends LogicTuple> tuples) {
        final Struct listOfTuples = PrologUtils.streamToList(tuples.stream().map(LogicTuple::getTuple));
        return request(HttpMethod.POST, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<Optional<LogicTuple>> tryTake(LogicTemplate template) {
        return request(HttpMethod.DELETE, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<Optional<LogicTuple>> tryRead(LogicTemplate template) {
        return request(HttpMethod.GET, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<LogicTuple> read(LogicTemplate template) {
        return request(HttpMethod.GET, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<LogicTuple> take(LogicTemplate template) {
        return request(HttpMethod.DELETE, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<LogicTuple> write(LogicTuple tuple) {
        return request(HttpMethod.POST, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public CompletableFuture<Collection<? extends LogicTuple>> get() {
        return request(HttpMethod.GET, null, query("someKey", "some value")) // TODO setup body and query args
                .thenApplyAsync(whathever -> null); // TODO handle me
    }

    @Override
    public String getName() {
        return name;
    }

    private static Pair<String, Object> query(String key, Object value) {
        return Pair.with(key, value);
    }

    // TODO notice me!
    private String getPath() {
        return String.format("/%s/prolog/%s", root, name);
    }

    /**
     * Converts an {@link Object#toString()} representation into an url-encoded string, using charset UTF-8
     * @param x the object whose {@link Object#toString()} representation must be url-encoded
     * @return an url-encoded string, using charset UTF-8
     */
    private static String urlEncode(Object x) {
        try {
            return URLEncoder.encode(x.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private CompletableFuture<String> request(HttpMethod method, Pair<String, Object>... queries) {
        return request(method, null, queries);
    }

    /**
     * Sends a HTTP request to the TuSoW service located on {@link #host}, listening on port {@link #port} and exposing
     * tuple spaces on the {@link #root} path
     *
     * @param method the HTTP method of the request
     * @param body the body of the HTTP request (may be null)
     * @param queries a possibly empty array of Key-Value pairs representing URL parameters. Values must be url-encoded
     *
     * @return a {@link CompletableFuture} optionally contaning the body of the server's HTTP response, if any,
     * decoded as UTF-8. Such future will be eventually completes as soon as the server's response is received by the client.
     * If some status code other than 2xx is returned, the future is exceptionally completed
     * with a new {@link UnexpectedResponseException} exception
     */
    private CompletableFuture<String> request(HttpMethod method, Object body, Pair<String, Object>... queries) {
        final String queryString = Arrays.stream(queries)
                .map(kv -> kv.getValue0() + "=" + urlEncode(kv.getValue1().toString()))
                .collect(Collectors.joining("&", "?", ""));

        final CompletableFuture<String> result = new CompletableFuture<>();

        final HttpClientRequest request = null;

        // TODO setup request

        if (body == null) {
            request.end();
        } else {
            request.end(body.toString());
        }

        return result;
    }


}
