package it.unibo.sd1819.lab9.agents;

import java.time.Duration;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.*;

// TODO look at my public methods and try to figure out how I should work!
public abstract class Agent {

    protected enum AndThen {
        CONTINUE, RESTART, PAUSE, STOP
    }

    protected enum States {
        CREATED, STARTED, RUNNING, PAUSED, STOPPED
    }

    private final String name;
    // TODO to be completed as soon as the agent stops
    private final CompletableFuture<Void> termination = new CompletableFuture<>();
    private States state = States.CREATED;
    private AndThen nextOperation = null;
    private Environment environment;

    protected Agent(String name) {
        this.name = Optional.ofNullable(name).orElseGet(() -> getClass().getSimpleName() + "#" + System.identityHashCode(Agent.this));
    }

    private void ensureCurrentStateIs(States state) {
        ensureCurrentStateIs(EnumSet.of(state));
    }

    private void ensureCurrentStateIs(EnumSet<States> states) {
        if (!currentStateIsOneOf(states)) {
            throw new IllegalStateException("Illegal state: " + this.state + ", expected: " + states);
        }
    }

    private boolean currentStateIs(States state) {
        return Objects.equals(this.state, state);
    }

    private boolean currentStateIsOneOf(EnumSet<States> states) {
        return states.contains(this.state);
    }

    private void doStateTransition(AndThen whatToDo) {
        switch (state) {
            case CREATED:
                doStateTransitionFromCreated(whatToDo);
                break;
            case STARTED:
                doStateTransitionFromStarted(whatToDo);
                break;
            case RUNNING:
                doStateTransitionFromRunning(whatToDo);
                break;
            case PAUSED:
                doStateTransitionFromPaused(whatToDo);
                break;
            case STOPPED:
                doStateTransitionFromStopped(whatToDo);
                break;
            default: throw new IllegalStateException("Illegal state: " + state);

        }
    }

    protected void doStateTransitionFromCreated(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromStarted(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromRunning(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromPaused(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }

    protected void doStateTransitionFromStopped(AndThen whatToDo){
        switch (whatToDo) {
            case PAUSE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case RESTART:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case STOP:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            case CONTINUE:
                // TODO handle this case
                throw new IllegalStateException("not implemented");
            default: throw new IllegalArgumentException("Unexpected transition: " + state + " -" + whatToDo + "-> ???");
        }
    }


    // TODO notice che convention doBegin --> begin --> onBegin

    private void doBegin() {
        if (getEnvironment() == null) {
            throw new IllegalStateException();
        }
        ensureCurrentStateIs(States.STARTED);
        getEngine().submit(this::begin);
    }

    private void begin() {
        nextOperation = AndThen.CONTINUE;
        try {
            onBegin();
        } catch (Exception e) {
            nextOperation = onError(e);
        } finally {
            doStateTransition(nextOperation);
        }
    }

    public void onBegin() throws Exception {
        // does nothing by default (must be inherited)
    }

    // TODO notice che convention doRun --> run --> onRun

    private void doRun() {
        ensureCurrentStateIs(EnumSet.of(States.PAUSED, States.RUNNING));
        getEngine().submit(this::run);
    }

    private void run() {
        // TODO implement me similarly to begin()
    }

    public abstract void onRun() throws Exception;

    // TODO notice che convention doEnd --> end --> onEnd

    private void doEnd() {
        // TODO implement me similarly to begin()
    }

    private void end() {
        // TODO implement me similarly to doBegin() and doRun()
    }

    public void onEnd() throws Exception {
        // does nothing by default (must be inherited)
    }

    // TODO notice che convention onError --> onUncaughtError

    private AndThen onError(Exception e) {
        if (e instanceof RestartException) {
            return AndThen.RESTART;
        } else if (e instanceof PauseException) {
            return AndThen.PAUSE;
        } else if (e instanceof StopException) {
            return AndThen.STOP;
        } else {
            return onUncaughtError(e);
        }
    }

    public AndThen onUncaughtError(Exception e) {
        // simply print stacktraces and then stops the agent (must be inherited)
        e.printStackTrace();
        return AndThen.STOP;
    }

    public final void start() {
        ensureCurrentStateIs(States.CREATED);
        // TODO state transition from CREATED to STARTED
    }

    protected final void resume() {
        ensureCurrentStateIs(States.PAUSED);
        // TODO state transition from CREATED to STARTED
    }

    protected final void pause() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        // TODO remember that the agent should move into the PAUSED state after the current onRun() or onBegin()
    }

    protected final void pauseNow() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        throw new PauseException(); 
        // state transition into the PAUSED state, interrupting the current onRun() or onBegin()
        // this must not be changed!
    }

    protected final void stop() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        if (currentStateIs(States.PAUSED)) {
            doStateTransition(AndThen.STOP);
        } else {
            nextOperation = AndThen.STOP;
        }
    }

    protected final void stopNow() {
        ensureCurrentStateIs(EnumSet.of(States.STARTED, States.RUNNING, States.PAUSED));
        throw new StopException(); // state transition into the STOPPED state, interrupting the current onRun() or onBegin()
        // this must not be changed!
    }

    protected final void restart() {
        ensureCurrentStateIs(EnumSet.complementOf(EnumSet.of(States.CREATED)));
        // TODO state transition from ANY_EXCEPT_CREATED to STARTED
    }

    protected final void restartNow() {
        ensureCurrentStateIs(EnumSet.complementOf(EnumSet.of(States.CREATED)));
        // TODO states the agent should move into the STARTED state after the current onRun()
    }

    protected Environment getEnvironment() {
        return environment;
    }

    protected ExecutorService getEngine() {
        return getEnvironment() != null ? getEnvironment().getEngine() : null;
    }

    protected void setEnvironment(Environment environment) {
        this.environment = Objects.requireNonNull(environment);
    }

    protected final void log(Object format, Object... args) {
        System.out.printf("[" + getName() +"] " + format + "\n", args);
    }

    public String getName() {
        return name;
    }

    public void await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(duration.toMillis(), TimeUnit.MILLISECONDS);
    }

    public void await() throws InterruptedException, ExecutionException, TimeoutException {
        termination.get(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }

    private static class RestartException extends RuntimeException { }

    private static class StopException extends RuntimeException { }

    private static class PauseException extends RuntimeException { }

}
